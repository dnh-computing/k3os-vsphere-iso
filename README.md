# k3OS VSphere ISO

A customization of the [k3OS](https://k3os.io/) ISO to allow non-interactive installation of k3OS in vSphere/vCenter/ESXi.

Download the ISO from the [latest pipeline build](https://gitlab.com/dnh-computing/k3os-vsphere-iso/-/jobs/artifacts/master/browse?job=build).

## Why?

The [automated installation instructions](https://github.com/rancher/k3os#automated-installation) require passing kernel
parameters, and most vSphere deployment automation (e.g. Terraform) doesn't support interacting with the Grub boot menu
to pass these custom parameters.

k3OS documents a [remastering ISO](https://github.com/rancher/k3os#remastering-iso) process to provide these custom parameters.

To avoid creating a custom ISO for every server, this repository creates an ISO that reads the k3OS configuration from the
VMWare `guestinfo.userdata` VM field, which is usually possible to set through automation (e.g. Terraform's
[extra_config](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#extra_config) argument).

## Example usage

1. Upload [the ISO](https://gitlab.com/dnh-computing/k3os-vsphere-iso/-/jobs/artifacts/master/browse?job=build) to your vSphere datastore
2. Create a VM with the ISO in its CD drive
3. Optionally, set the `guestinfo.userdata` VM configuration field to the base64-encoded version of your k3os YAML configuration file

e.g. in Terraform:
```

resource "vsphere_virtual_machine" "my-k3os" {
  name             = "my-k3os"
  resource_pool_id = "resgroup-55"
  datastore_id     = "datastore-42"

  num_cpus             = 8
  num_cores_per_socket = 8
  memory               = 16384

  guest_id  = "ubuntu64Guest"
  scsi_type = "lsilogic"

  network_interface {
    network_id = "dvportgroup-123"
  }

  disk {
    label = "disk0"
    size  = 16
  }

  cdrom {
    datastore_id = "datastore-42"
    path         = "Images/k3os-vx.y.z-custom.iso"
  }

  extra_config = {
    "guestinfo.userdata" = base64encode(yamlencode({
      hostname = "my-k3os"
      k3os = {
        password = "$6$.0SZHCu6c0GZVitY$UQC3PCj1xt8miYF8370oK72xtuPNaiZ8zoKk1MlszhYgO9aOKrOm3v0unW1x/Ikt2qOUfZqNJF3.4wvxFyI7V/" # rancher
      }
    }))
  }
}
```

## License

k3OS is copyright (c) [Rancher Labs, Inc.](http://rancher.com)

This repository is copyright (c) 2020 Nicholas Hinds

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
